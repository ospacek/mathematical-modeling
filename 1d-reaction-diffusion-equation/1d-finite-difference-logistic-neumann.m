clc
clear
close all

#---settings--------------------------------------------------------------------
I = [0, 1];
dx = 0.01;
order = (I(2) - I(1)) / dx + 1; 
A = zeros(order, order);

#---generate-matrix-------------------------------------------------------------
A(1,1) = -2/(dx^2); A(1,2) = 2/(dx^2);
for i = 2:order-1
  # printf("row %d\n", i)
  for j = i-1:i+1
    # printf("\t column %d\n", j)
    A(i,j) = 1/(dx^2);
    if j == i
      A(i,j) = -2/(dx^2);
    endif
  endfor
endfor
A(order,order-1) = 2/(dx^2); A(order,order) = -2/(dx^2);

#---ode-solver------------------------------------------------------------------
x = I(1):dx:I(2);
#ic = x.*(1-x);  # nekompatibilní počáteční podmínka
#ic = cos(pi*x); # kompatibilní počáteční podmínka (uteče do -oo)
#ic = abs(cos(pi*x));
ic = cos(pi*x) + 1; # kompatibilní počáteční podmínka

function rhs = odefun(t, u, A)
  rhs = A*u + u.*(1-u);
endfunction

t_cmp = [0, 20]
[t,sol] = ode15s(@(t,u) odefun(t, u, A), t_cmp, ic);

#---plot------------------------------------------------------------------------
figure(1);
x = I(1):dx:I(2)
plot(x, sol(1, :), "displayname", "initial condition", "linewidth", 1.3)
hold on
plot(x, sol(30, :), "displayname", "u(x, 0.14194)", "linewidth", 1.3)
hold on
plot(x, sol(end, :), "displayname", "solution", "linewidth", 1.3)
title("Homogeneous Boundary Neumann Condition for one 1D equation")
xlabel("x")
ylabel("u")
legend()
