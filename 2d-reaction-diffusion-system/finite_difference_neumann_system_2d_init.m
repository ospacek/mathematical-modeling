clc;
clear;
close all;

%---settings---------------------------------------------------------------
I = [0, 1];

params.dx = 0.02;
params.a = 0.2;
params.b = 2;
params.d1 = 0.0002;
params.d2 = 0.02;
params.m = (I(2) - I(1)) / params.dx - 1;

%---generate-local-matrix--------------------------------------------------
L = zeros(params.m + 2, params.m + 2);
L(1,1) = -4; L(1,2) = 2;

for i = 2:params.m+1
  for j = i-1:i+1
    L(i,j) = 1;
    if j == i
      L(i,j) = -4;
    end
  end
end
L(params.m+2,params.m+1) = 2; L(params.m+2,params.m+2) = -4;

%---generate-auxiliary-matrix----------------------------------------------
P = zeros((params.m+2)^2, (params.m+2)^2);
P(1:params.m + 2,1:params.m + 2) = L;
P(1:params.m + 2,params.m + 3: 2 * (params.m + 2)) = 2 * eye(params.m + 2);
for i = 2:params.m+1
  for j = i-1:i+1 
    row_s = (i-1)*(params.m+2) + 1; col_s = (j-1)*(params.m+2) + 1;
    row_e = row_s + params.m + 1;
    col_e = col_s + params.m + 1;
    P(row_s:row_e, col_s:col_e) = eye(params.m + 2);
    if j == i
      P(row_s:row_e, col_s:col_e) = L;
    end
  end
end
e = (params.m + 2)^2; s = e - (params.m + 1);
P(s:e,s:e) = L;
P(s:e,s-(params.m+2):s-1) = 2 * eye(params.m + 2);

P = sparse(P);
P = P ./ params.dx^2;

%---ode-solver-------------------------------------------------------------
rand_matrix = rand(params.m + 2, params.m + 2) / 10;
up = params.a + params.b;
vp = params.b / (params.a + params.b)^2;
ic_u = rand_matrix + up;
ic_v = rand_matrix + vp;

t_cmp = [0, 10000];

