close all;

figure(1);
surf(x, y, sol_u)
shading interp
title("Homogeneous Boundary Neumann Condition for 2D System - solution for u")
xlabel("x")
ylabel("y")
%colormap gray

figure(2);
surf(x, y, sol_v)
shading interp
title("Homogeneous Boundary Neumann Condition for 2D System - solution for v")
xlabel("x")
ylabel("y")
%colormap gray
