[t,sol] = ode15s(@(t,u) odefun(t, u, P, params), t_cmp, [reshape(ic_u, 1, (params.m + 2)^2), reshape(ic_v, 1, (params.m + 2)^2)]);

sol_u = sol(:,1:(params.m+2)^2);
sol_v = sol(:,(params.m+2)^2 + 1:end);

x = I(1):params.dx:I(2);
y = x;
sol_u = reshape(sol_u(end,:), params.m + 2, params.m + 2);
sol_v = reshape(sol_v(end,:), params.m + 2, params.m + 2);

function rhs = odefun(t, u, P, params)
  u = reshape(u, (params.m+2)^2, 2);
  U = u(:, 1);
  V = u(:, 2);
  
  Gu = params.d1 .* P*U + params.a - U + U.^2 .* V;
  Gv = params.d2 .* P*V + params.b - U.^2 .* V;
  
  rhs = [Gu; Gv];
end