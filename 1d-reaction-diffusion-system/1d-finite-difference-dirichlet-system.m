clc
clear
close all

#---settings--------------------------------------------------------------------
I = [0, 1];

params.dx = 0.005;
params.a = 0.2;
params.b = 2;
params.alpha = params.a + params.b;
params.beta = params.b / (params.a + params.b)^2;
params.d1 = 0.00005;
params.d2 = 0.05;
params.m = (I(2) - I(1)) / params.dx - 1;

A = zeros(params.m + 2, params.m + 2);

#---generate-matrix-------------------------------------------------------------
for i = 2:params.m+1
  # printf("row %d\n", i)
  for j = i-1:i+1
    # printf("\t column %d\n", j)
    A(i,j) = 1/(params.dx^2);
    if j == i
      A(i,j) = -2/(params.dx^2);
    endif
  endfor
endfor
A = A(2:end-1, 2:end-1);

#---ode-solver------------------------------------------------------------------
x = I(1):params.dx:I(2);
rand_vect = rand(1, params.m + 2) / 10;
up = params.a + params.b;
vp = params.b / (params.a + params.b)^2
ic_u = rand_vect + up;
ic_v = rand_vect + vp;

function rhs = odefun(t, u, A, params)
  u = reshape(u, params.m, 2);
  u1 = u(:, 1);
  u2 = u(:, 2);
  
  bdu = zeros(rows(u1), 1);
  bdu(1) = params.d1 * params.alpha / params.dx^2;
  bdu(end) = params.d1 * params.alpha / params.dx^2;
  
  bdv = zeros(rows(u2), 1);
  bdv(1) = params.d2 * params.beta / params.dx^2;
  bdv(end) = params.d2 * params.beta / params.dx^2;
  
  A1 = params.d1 * A*u1 + params.a - u1 + u1.^2 .* u2 + bdu;
  A2 = params.d2 * A*u2 + params.b - u1.^2 .* u2 + bdv;

  rhs = [A1; A2];
endfunction

ic = [ic_u(2:end-1), ic_v(2:end-1)];
t_cmp = [0, 10000];
[t,sol] = ode15s(@(t,u) odefun(t, u, A, params), t_cmp, ic);

sol_u = sol(:, 1:params.m);
sol_u = [ones(rows(sol), 1) * params.alpha sol_u ones(rows(sol), 1) * params.alpha];
sol_v = sol(:, params.m + 1:end);
sol_v = [ones(rows(sol), 1) * params.beta sol_v ones(rows(sol), 1) * params.beta];

#---plot------------------------------------------------------------------------
x = I(1):params.dx:I(2);

figure(1);
plot(x, ic_u, "displayname", "initial condition", "linewidth", 1.3)
hold on
plot(x, sol_u(100, :), "displayname", strcat("u(x, ", num2str(t(100)), ")"), "linewidth", 1.3)
hold on
plot(x, sol_u(end, :), "displayname", "solution", "linewidth", 1.3)
title("Boundary Dirichlet Condition for 1D system")
xlabel("x")
ylabel("u")
legend()

figure(2);
plot(x, ic_v, "displayname", "initial condition", "linewidth", 1.3)
hold on
plot(x, sol_v(100, :), "displayname", strcat("v(x, ", num2str(t(100)), ")"), "linewidth", 1.3)
hold on
plot(x, sol_v(end, :), "displayname", "solution", "linewidth", 1.3)
title("Boundary Dirichlet Condition for 1D system")
xlabel("x")
ylabel("v")
legend()
