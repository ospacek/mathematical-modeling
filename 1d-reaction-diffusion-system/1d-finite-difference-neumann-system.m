clc
clear
close all

#---settings--------------------------------------------------------------------
I = [0, 1];

params.dx = 0.005;
params.a = 0.2;
params.b = 2;
params.d1 = 0.00005;
params.d2 = 0.05;
params.m = (I(2) - I(1)) / params.dx - 1;

A = zeros(params.m + 2, params.m + 2);

#---generate-matrix-------------------------------------------------------------
A(1,1) = -2/(params.dx^2); A(1,2) = 2/(params.dx^2);
for i = 2:params.m+1
  # printf("row %d\n", i)
  for j = i-1:i+1
    # printf("\t column %d\n", j)
    A(i,j) = 1/(params.dx^2);
    if j == i
      A(i,j) = -2/(params.dx^2);
    endif
  endfor
endfor
A(params.m+2,params.m+1) = 2/(params.dx^2); A(params.m+2,params.m+2) = -2/(params.dx^2);

#---ode-solver------------------------------------------------------------------
x = I(1):params.dx:I(2);
rand_vect = rand(1, params.m + 2) / 10;
up = params.a + params.b;
vp = params.b / (params.a + params.b)^2;
ic_u = rand_vect + up;
ic_v = rand_vect + vp;

function rhs = odefun(t, u, A, params)
  u = reshape(u, params.m+2, 2);
  u1 = u(:, 1);
  u2 = u(:, 2);

  A1 = params.d1 * A*u1 + params.a - u1 + u1.^2 .* u2;
  A2 = params.d2 * A*u2 + params.b - u1.^2 .* u2;

  rhs = [A1; A2];
endfunction

t_cmp = [0, 10000];
[t,sol] = ode15s(@(t,u) odefun(t, u, A, params), t_cmp, [rand_vect + up, rand_vect + vp]);

sol_u = sol(:,1:params.m+2);
sol_v = sol(:,params.m+3:end);

#---plot------------------------------------------------------------------------
x = I(1):params.dx:I(2);

figure(1);
plot(x, ic_u, "displayname", "initial condition", "linewidth", 1.3)
hold on
plot(x, sol_u(69, :), "displayname", strcat("u(x, ", num2str(t(69)), ")"), "linewidth", 1.3)
hold on
plot(x, sol_u(end, :), "displayname", "solution", "linewidth", 1.3)
title("Homogeneous Boundary Neumann Condition for 1D system")
xlabel("x")
ylabel("u")
legend()

figure(2);
plot(x, ic_v, "displayname", "initial condition", "linewidth", 1.3)
hold on
plot(x, sol_v(69, :), "displayname", strcat("v(x, ", num2str(t(69)), ")"), "linewidth", 1.3)
hold on
plot(x, sol_v(end, :), "displayname", "solution", "linewidth", 1.3)
title("Homogeneous Boundary Neumann Condition for 1D system")
xlabel("x")
ylabel("v")
legend()
